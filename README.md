## Environment variables
* `SLAPD_DOMAIN`: suffix in the form "dc=example,dc=com"
* `SLAPD_PASSWORD`: Set root password
* `SLAPD_PASSWORD_FILE`: File containing root password (useful for docker secrets)

## SSL
Not working yet. I don't know if I will keep this part since I created this image to be used from inside a docker network.

## Initialize database with existing ldif files
Mount ldif files into `/docker-entrypoint.d/` directory to add these to the database.

## docker-compose.yml

Here are some examples:

### Openldap service
```yaml
version: "3.5"

volumes:
  conf:
  data:

secrets:
  openldap-root-password:
    external: true

configs:
  init-ldif:
    file: ./01-init.ldif

services:
  openldap:
    image: registry.gitlab.com/salokyn/docker-openldap
    volumes:
      - conf:/etc/openldap
      - data:/var/lib/openldap
    configs:
      - source: init-ldif
        target: /docker-entrypoint.d/01-init.ldif
    environment:
      - SLAPD_DOMAIN=dc=mydomain,dc=com
      - SLAPD_PASSWORD_FILE=/run/secrets/openldap-root-password
```

### App using LDAP

Note `networks` configuration:

```yaml
version: '3.5'

volumes:
  app:
  db:
  data:
  config:

services:
  db:
    image: mariadb
    restart: on-failure
    volumes:
      - db:/var/lib/mysql
    env_file:
      - db.env

  app:
    image: nextcloud:stable-fpm-alpine
    volumes:
      - nextcloud:/var/www/html
      - config:/var/www/html/config
      - data:/var/www/html/data
    restart: on-failure
    environment:
      - MYSQL_HOST=db
      - NEXTCLOUD_TRUSTED_DOMAIN=mydomain.com
      - NEXTCLOUD_ADMIN_USER=admin
      - NEXTCLOUD_ADMIN_PASSWORD=mySecretPassword
    env_file:
      - db.env
    networks:
      - default
      - openldap_default
    depends_on:
      - db

  web:
    image: nginx:alpine
    ports:
      - 80:80
    volumes:
      - ./default.conf:/etc/nginx/conf.d/default.conf:ro
      - nextcloud:/var/www/html:ro
    restart: on-failure
    depends_on:
      - app

networks:
  openldap_default:
    external: true
```
