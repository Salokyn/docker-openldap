#!/bin/sh
set -ex

SLAPD_CONF=/etc/openldap/slapd.conf
INITIALIZED=/etc/openldap/.initialized

replace() {
  sed -i "/^$1/ s#$1\(\s*\).*#$1\1$2#" "$3"
}

# Limit open files descriptors. See https://github.com/docker/docker/issues/8231
# shellcheck disable=SC3045
ulimit -n 1024

if [ -n "$SSL" ]; then
  PRIVATE_KEY=/etc/ldap/ssl/private.key
  CERT=/etc/ldap/ssl/cert.pem

  if [ ! -f "$PRIVATE_KEY" ]; then
    openssl genrsa -out "$PRIVATE_KEY"  2048
  fi

  if [ ! -f "$CERT" ]; then
    openssl req -new -x509 -key "$PRIVATE_KEY" -out "$CERT" -days 3652
  fi

  if (grep -q TLSCACertificateFile "$SLAPD_CONF"); then
    replace TLSCACertificateFile "$CERT" "$SLAPD_CONF"
  else
    echo "TLSCACertificateFile $CERT" >> "$SLAPD_CONF"
  fi

  if (grep -q TLSCertificateFile "$SLAPD_CONF"); then
    replace TLSCertificateFile "$CERT" "$SLAPD_CONF"
  else
    echo "TLSCertificateFile $CERT" >> "$SLAPD_CONF"
  fi

  if (grep -q TLSCertificateKeyFile "$SLAPD_CONF"); then
    replace TLSCertificateKeyFile "$PRIVATE_KEY" "$SLAPD_CONF"
  else
    echo "TLSCertificateKeyFile $PRIVATE_KEY" >> "$SLAPD_CONF"
  fi
fi

if [ ! -f "$INITIALIZED" ]; then
  # Set domain
  if [ -n "$SLAPD_DOMAIN" ]; then
    replace suffix "\"$SLAPD_DOMAIN\"" "$SLAPD_CONF"
    sed -i "/^rootdn/ s/rootdn\(\s*\)\"cn=\(\w\+\),.*/rootdn\1\"cn=\2,$SLAPD_DOMAIN\"/" "$SLAPD_CONF"
  fi

  # Set password
  if [ -n "$SLAPD_PASSWORD" ]; then
    replace rootpw "$(slappasswd -h '{SSHA}' -s "$SLAPD_PASSWORD")" "$SLAPD_CONF"
  elif [ -f "$SLAPD_PASSWORD_FILE" ]; then
    replace rootpw "$(slappasswd -h '{SSHA}' -T "$SLAPD_PASSWORD_FILE")" "$SLAPD_CONF"
  fi

  find /docker-entrypoint.d/ -name '*.ldif' -type f -print0|xargs -rt0n1 slapadd -vl && touch "$INITIALIZED"
fi

chown -R ldap:ldap /var/lib/openldap

# Update index
slapindex -b "$SLAPD_DOMAIN"

exec "$@"
