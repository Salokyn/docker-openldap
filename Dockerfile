FROM alpine

RUN apk add --no-cache \
      openldap \
      openldap-back-mdb \
      openldap-overlay-memberof; \
    \
    mkdir /docker-entrypoint.d; \
    \
    mkdir /run/openldap; \
    chown ldap:ldap /run/openldap

COPY slapd.conf /etc/openldap/slapd.conf

VOLUME /etc/openldap /var/lib/openldap

EXPOSE 389

COPY entrypoint.sh .

ENTRYPOINT ["/bin/sh","/entrypoint.sh"]
CMD ["/usr/sbin/slapd","-d","32768","-g","ldap","-u","ldap"]
